﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockerPieceObject : MonoBehaviour
{
    public Piece Piece;

    public int Health;

    [SerializeField] private SpriteRenderer _spriteRenderer;

    public void SetSprite(Sprite sprite)
    {
        _spriteRenderer.sprite = sprite;
    }

    public void CreateBlocker(Piece piece)
    {
        SetSprite(piece.PieceSprites[0]);
        Health = piece.MaxHealth;
    }
}