using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObject : MonoBehaviour
{
    private Tile _tile;
    public SpriteRenderer BoardRenderer;

    public void SetTile(Tile tile)
    {
        _tile = tile;
    }

    public Tile GetTile()
    {
        return _tile;
    }

    public void Interact ()
    {
        _tile.Interact();
    }
}
