﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidPieceObject : MonoBehaviour
{
    public Piece Piece { get; set; }
    public int Health { get; set; }

    [SerializeField] private SpriteRenderer _pieceRenderer;

    public void SetSprite(Sprite sprite)
    {
        _pieceRenderer.sprite = sprite;
    }

    public void CreatePiece(Piece piece)
    {
        Piece = piece;
        SetSprite(piece.PieceSprites[0]);
        Health = piece.MaxHealth;
    }
}