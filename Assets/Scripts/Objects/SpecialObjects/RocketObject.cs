﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RocketObject : MonoBehaviour
{
    [SerializeField] private float _power = 100;
    [SerializeField] private float _destructionTimer = 1.5f;

    [SerializeField] private Rigidbody2D _rbOne;
    [SerializeField] private Rigidbody2D _rbTwo;

    public bool IsHor;

    public void Shoot ()
    {
        _rbOne.GetComponent<CollisionEvent>().Init(TileCollision);
        _rbTwo.GetComponent<CollisionEvent>().Init(TileCollision);

        if (IsHor)
        {
            _rbOne.AddForce(-transform.right * _power);
            _rbTwo.AddForce(transform.right * _power);
        }
        else
        {
            _rbOne.AddForce(transform.up * _power);
            _rbTwo.AddForce(-transform.up * _power);
        }

        StartCoroutine(DestructionTimer());
    }

    private IEnumerator DestructionTimer()
    {
        yield return new WaitForSeconds(_destructionTimer);
        InputManager.instance.RemoveInputBlocker();
        Destroy(gameObject);
    }

    public void TileCollision(Collider2D collision)
    {
        TileObject tileObject = collision.GetComponent<TileObject>();

        if (tileObject == null)
            return;

        Tile tile = tileObject.GetTile();

        if (tile.SolidPieceType == PieceType.RocketHorizontal || tile.SolidPieceType == PieceType.RocketVertical)
            tile.SetPiece(IsHor ? PieceType.RocketVertical : PieceType.RocketHorizontal);

        tile.DirectHit(true);

        if (IsHor)
        {
            tile.Neighbours[1]?.AdjacentHit(true);
            tile.Neighbours[3]?.AdjacentHit(true);
        }
        else
        {
            tile.Neighbours[0]?.AdjacentHit(true);
            tile.Neighbours[2]?.AdjacentHit(true);
        }
    }
}
