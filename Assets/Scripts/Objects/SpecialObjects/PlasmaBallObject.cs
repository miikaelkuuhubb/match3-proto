﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaBallObject : MonoBehaviour
{
    [SerializeField] private float _targetInterval;
    [SerializeField] private float _pauseTime;
    [SerializeField] private float _activationInterval;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    private int _specialCombined = 0; //0 = no combination, 1= rocket, 2 = bomb
    public GameObject LinePrefab;
    private List<GameObject> PlasmaLines = new List<GameObject>();

    public void Activate(Tile tile, PieceType targetPieceType, Sprite plasmaSprite, int specialCombined)
    {
        _specialCombined = specialCombined;
        _spriteRenderer.sprite = plasmaSprite;
        List<Tile> targetTiles = tile.GridData.FindAllOfPieceType(targetPieceType);
        StartCoroutine(PlasmaAnimation(tile, targetTiles));
    }

    public IEnumerator PlasmaAnimation(Tile tile, List<Tile> targetTiles)
    {
        foreach (Tile t in targetTiles)
        {
            GameObject newPlasmaLine = Instantiate(LinePrefab, tile.TileObject.transform.position, Quaternion.identity);
            PlasmaLines.Add(newPlasmaLine);
            newPlasmaLine.GetComponent<PlasmaBallLine>().SetTargetPositions(this.gameObject.transform.position, t.TileObject.gameObject.transform.position, t);
            Debug.Log("PlasmaLaserHit!");
            yield return new WaitForSeconds(_targetInterval);
        }

        yield return new WaitForSeconds(_pauseTime);

        foreach (GameObject p in PlasmaLines)
        {
            Destroy(p);
        }

        foreach (Tile t in targetTiles)
        {
            t.DirectHit(true);
            if (_specialCombined == 0)
            {
                foreach (Tile p in t.Neighbours) //If not a special combination, spawn a random tile to hit tiles' neighbors and do special adjacent damage
                {
                    if (p != null)
                    {
                        p.AdjacentHit(true);

                        if (p.SolidPieceType == PieceType.Empty)
                            p.Create();
                    }
                }
            }

            if (_specialCombined != 0) //If specialcombination, tiles should be activated one by one instead of at the same time
                yield return new WaitForSeconds(_activationInterval);
        }

        tile.Destroy();
        InputManager.instance.RemoveInputBlocker();
        Destroy(gameObject);
    }
}
