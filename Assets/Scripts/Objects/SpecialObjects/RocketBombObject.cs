﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RocketBombObject : MonoBehaviour
{
    [SerializeField] private float _destructionTimer = 1.5f;

    [SerializeField] private List<GameObject> rocketList = new List<GameObject>();


    public void Activate()
    {
        foreach(GameObject p in rocketList)
        {
            p.GetComponent<RocketObject>().Shoot();
        }

        StartCoroutine(DestructionTimer());
    }

    private IEnumerator DestructionTimer()
    {
        yield return new WaitForSeconds(_destructionTimer);
        InputManager.instance.RemoveInputBlocker();
        Destroy(gameObject);
    }

    
}
