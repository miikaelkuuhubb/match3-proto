﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaBallLine : MonoBehaviour
{
	public int amountOfPoints;
	public Vector3[] startPositions;
	public Vector3[] linePositions;
	public LineRenderer _lineRenderer;
	public Tile targetTile;
	public float lineSpeed;
	public float wiggle;
	public float maxWiggle;

	// Use this for initialization
	void Start()
	{
		_lineRenderer.positionCount = 1;
	}

	// Update is called once per frame
	void Update()
	{
		for (int i = 0; i < _lineRenderer.positionCount; i++)
		{
			if (i != 0 && i != linePositions.Length - 1)
			{
				linePositions[i] = new Vector3(
					(Mathf.Clamp(linePositions[i].x + Random.Range(-wiggle, wiggle), startPositions[i].x - maxWiggle, startPositions[i].x + maxWiggle)),
					(Mathf.Clamp(linePositions[i].y + Random.Range(-wiggle, wiggle), startPositions[i].y - maxWiggle, startPositions[i].y + maxWiggle)),
					(Mathf.Clamp(linePositions[i].y + Random.Range(-wiggle, wiggle), startPositions[i].z - maxWiggle, startPositions[i].z + maxWiggle)));
				_lineRenderer.SetPosition(i, linePositions[i]);
			}
		}
	}

	public void SetTargetPositions(Vector3 startPos, Vector3 targetPos, Tile givenTile)
	{
		targetTile = givenTile;
		StartCoroutine(ProgressLine(startPos, targetPos));
	}

	IEnumerator ProgressLine(Vector3 startPos, Vector3 targetPos)
	{
		_lineRenderer = GetComponent<LineRenderer>();
		startPositions = new Vector3[amountOfPoints];
		linePositions = new Vector3[amountOfPoints];
		Vector3 difference = targetPos - startPos;
		difference = (difference / amountOfPoints);

		for (int i = 0; i < amountOfPoints; i++)
		{
			_lineRenderer.positionCount += 1;
			startPositions[i] = (startPos + difference * i);
			linePositions[i] = (startPos + difference * i);
			_lineRenderer.SetPosition(i, linePositions[i]);
			yield return new WaitForSeconds(0.001f);
		}

		//targetTile.PlayAnimation("Stared"); //TODO: TILE'S PIECE SHOULD START PLAYING SHAKING ANIMATION HERE, BECAUSE A LINE REACHES IT!

		yield return null;
	}
}