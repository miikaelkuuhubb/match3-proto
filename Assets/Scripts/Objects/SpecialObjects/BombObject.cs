﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombObject : MonoBehaviour
{
    [SerializeField] private float _maxColliderRadius = 2f;
    [SerializeField] private float _explodeTime = 1f;
    [SerializeField] private AnimationCurve _explodeAnimationCurve;

    private CircleCollider2D _col;

    public void Explode()
    {
        _col = GetComponent<CircleCollider2D>();
        StartCoroutine(ExplodeAnimation());
    }

    public IEnumerator ExplodeAnimation()
    {
        yield return new WaitForSeconds(0.5f);

        _col.enabled = true;

        float elapsedTime = 0f;
        while (elapsedTime <= _explodeTime)
        {
            _col.radius = Mathf.LerpUnclamped(0, _maxColliderRadius, _explodeAnimationCurve.Evaluate(elapsedTime / Time.deltaTime));
            elapsedTime += Time.deltaTime;
        }

        yield return new WaitForSeconds(0.3f);

        InputManager.instance.RemoveInputBlocker();

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        TileObject tileObject = collision.GetComponent<TileObject>();
        if (tileObject == null)
            return;

        tileObject.GetTile().DirectHit(true);
    }
}
