﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GridData : ScriptableObject
{
    public List<Tile> Tiles = new List<Tile>();

    [Header("Tile Types")]
    [SerializeField] private List<Piece> PieceTypes = new List<Piece>();

    [Header("Prefabs")]
    public GameObject TilePrefab;
    public GameObject PiecePrefab;
    public GameObject BlockerPrefab;

    [Header("Special Prefabs")]
    public GameObject RocketHorizontalPrefab;
    public GameObject RocketVerticalPrefab;
    public GameObject BombPrefab;
    public GameObject PlasmaPrefab;

    [Header("Special Combination Prefabs")]
    public GameObject RocketRocketObject;
    public GameObject RocketBombObject;
    public GameObject BombBombObject;
    public GameObject PlasmaPlasmaObject;

    public void Clear() => Tiles.Clear();

    public Piece GetPieceOfType(PieceType pieceType) => PieceTypes.Find(p => pieceType == p.PieceType);

    public List<Tile> FindColumn(int column) => Tiles.FindAll(t => (int)t.Coordinates.y == column);

    public List<Tile> FindRow(int row) => Tiles.FindAll(t => (int)t.Coordinates.x == row);

    public List<Tile> GetTilesWithinRange(Tile tile, int range) => Tiles.FindAll(t => GetDistance(tile, t) <= range);

    public Tile Find(Predicate<Tile> match) => Tiles.Find(match);

    public List<Tile> FindAll(Predicate<Tile> match) => Tiles.FindAll(match);

    public List<Tile> FindAllOfPieceType(PieceType pieceType) => Tiles.FindAll(t => t.SolidPiece.PieceType == pieceType || t.SolidPiece.GetType().IsSubclassOf(GetPieceOfType(pieceType).GetType()));

    public int GetDistance(Tile originTile, Tile targetTile) => (int)(Mathf.Abs(originTile.Coordinates.x - targetTile.Coordinates.x) + Mathf.Abs(originTile.Coordinates.y - targetTile.Coordinates.y));
}