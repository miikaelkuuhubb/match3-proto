﻿using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public GridData GridData { get; private set; }

    //Tile Properties
    public Vector2 Coordinates { get; private set; }
    public Tile[] Neighbours { get; set; }
    public TileObject TileObject { get; private set; }

    //Solid Piece Properties
    private SolidPieceObject _pieceObject { get; set; }

    public Piece SolidPiece { get { return _pieceObject.Piece; } private set { _pieceObject.Piece = value; } }
    public PieceType SolidPieceType { get { return _pieceObject.Piece.PieceType; } }
    public int SolidPieceHealth { get { return _pieceObject.Health; } set { _pieceObject.Health = value; } }
    public Sprite PieceSprite { set { _pieceObject.SetSprite(value); } }

    //Foreground Blocker Properties
    private List<BlockerPieceObject> _blockerObjects { get; set; }
    public Piece BlockerPiece { get { return _blockerObjects[0].Piece; } }

    public List<Piece> AllBlockerPieces
    {
        get
        {
            List<Piece> blockers = new List<Piece>();
            foreach (BlockerPieceObject blockerObject in _blockerObjects)
            {
                blockers.Add(blockerObject.Piece);
            }
            return blockers;
        }
    }

    public PieceType BlockerPieceType { get { return _blockerObjects[0].Piece.PieceType; } }
    public int TopBlockerHealth { get { return _blockerObjects[0].Health; } set { _blockerObjects[0].Health = value; } }
    public Sprite TopBlockerSprite { set { _blockerObjects[0].SetSprite(value); } }
    public int BlockerCount { get { return _blockerObjects.Count; } }

    public Tile(Vector2 coordinates, TileObject tileObject, GridData gridData, SolidPieceObject pieceObject, List<BlockerPieceObject> blockers)
    {
        Coordinates = coordinates;
        _pieceObject = pieceObject;
        TileObject = tileObject;
        GridData = gridData;
        _blockerObjects = blockers;
    }

    #region Object Handling

    public void SetPiece(PieceType pieceType)
    {
        Piece piece = VerifyPieceType(pieceType);

        SolidPiece = piece;
    }

    public void CreateSolidPiece(PieceType pieceType)
    {
        Piece piece = VerifyPieceType(pieceType);

        _pieceObject.CreatePiece(piece);
    }

    public void CreateBlockerPiece(PieceType pieceType)
    {
        Piece piece = VerifyPieceType(pieceType);

        GameObject newBlockerGameObject = GameObject.Instantiate(GridData.BlockerPrefab, TileObject.transform);
        newBlockerGameObject.transform.localPosition = new Vector3(0, 0, -1f);

        BlockerPieceObject newBlockerObject = newBlockerGameObject.GetComponent<BlockerPieceObject>();
        newBlockerObject.Piece = piece;

        newBlockerObject.CreateBlocker(piece);
        _blockerObjects.Add(newBlockerObject);

        newBlockerObject.GetComponent<SpriteRenderer>().sortingOrder = 6 + _blockerObjects.IndexOf(newBlockerObject);
    }

    public void RemoveBlockerPiece(PieceType pieceType)
    {
        Piece piece = VerifyPieceType(pieceType);

        Debug.Log("destroy blocker!");
        BlockerPieceObject toRemove = _blockerObjects.Find(b => b.Piece == GridData.GetPieceOfType(pieceType));

        if (toRemove == null)
        {
            throw new MissingReferenceException("Blocker of PieceType " + pieceType + " not found in Tile at Coordinates " + Coordinates);
        }

        VictoryConditions.instance.UpdateConditions(BlockerPieceType);

        _blockerObjects.Remove(toRemove);
        GameObject.Destroy(toRemove.gameObject);

        foreach (BlockerPieceObject blocker in _blockerObjects)
        {
            blocker.GetComponent<SpriteRenderer>().sortingOrder = 6 + _blockerObjects.IndexOf(blocker);
        }
    }

    private Piece VerifyPieceType(PieceType pieceType)
    {
        Piece piece = GridData.GetPieceOfType(pieceType);

        if (piece == null)
        {
            throw new MissingReferenceException("Piece of PieceType " + pieceType + " not found in GridData.");
        }

        return piece;
    }

    #endregion

    #region Abstract Piece Method Calls

    public Piece TopPiece
    {
        get
        {
            if (_blockerObjects.Count > 0)
            {
                Debug.Log("Blocker!");
                return BlockerPiece;
            }
            else
            {
                return SolidPiece;
            }
        }
    }

    public void Interact()
    {
        TopPiece.Interact(this);
    }

    public void AdjacentHit(bool specialHit)
    {
        TopPiece.AdjacentHit(this, specialHit);
    }

    public void DirectHit(bool specialHit)
    {
        TopPiece.DirectHit(this, specialHit);
    }

    public void Match(Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles = null)
    {
        TopPiece.Match(this, originPiece, matchedTiles, adjacentTiles);
    }

    public void Create()
    {
        SolidPiece.Create(this);
    }

    public void Destroy()
    {
        VictoryConditions.instance.UpdateConditions(SolidPieceType);

        TopPiece.Destroy(this);
    }

    #endregion
}