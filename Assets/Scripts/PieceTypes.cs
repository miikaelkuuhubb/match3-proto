﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceTypes : ScriptableObject
{
    [Header("Piece Types")]
    [SerializeField] private List<Piece> Types = new List<Piece>();

    [Header("Prefabs")]
    public GameObject TilePrefab;
    public GameObject PiecePrefab;
    public GameObject BlockerPrefab;
}
