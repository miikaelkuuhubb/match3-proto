﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType
{
    //BaseTypes
    Empty,
    Basic,
    Special,
    SolidBlocker,
    ForegroundBlocker,

    //Colors
    RandomColor,
    Blue,
    Red,
    Green,
    Yellow,
    Orange,
    LightBlue,

    //Specials
    RandomSpecial,
    RandomRocket,
    RocketHorizontal,
    RocketVertical,
    Bomb,
    RandomPlasma,
    PlasmaBlue,
    PlasmaRed,
    PlasmaGreen,
    PlasmaYellow,
    PlasmaOrange,
    PlasmaLightBlue,

    //Special Combinations
    RocketBomb,
    RocketPlasma,
    RocketRocket,
    BombBomb,
    BombPlasma,
    PlasmaPlasma,

    //Solid Blockers
    PaperPile,
    Popcorn,
    LightBulb,

    //Foreground Blockers
    Rope,
    Trap,
    Piece
}

[Serializable]
public class LevelData
{
    public int MoveLimit;
    public List<RoomData> Rooms = new List<RoomData>();
    public List<TileData> Tiles = new List<TileData>();

    public LevelData(int moveLimit, List<RoomData> rooms, List<TileData> tiles)
    {
        MoveLimit = moveLimit;
        Rooms = rooms;
        Tiles = tiles;
    }
}

[Serializable]
public class RoomData
{
    public Vector2 RoomCoordinate;
    public List<VictoryCondition> VictoryConditions;

    public RoomData(Vector2 roomCoordinate, List<VictoryCondition> victoryConditions)
    {
        RoomCoordinate = roomCoordinate;
        VictoryConditions = victoryConditions;
    }
}

[Serializable]
public class TileData
{
    public Vector2 Coordinates;
    public PieceType SolidPieceType;
    public List<PieceType> ForegroundBlockerTypes;

    public TileData(Vector2 coordinates, PieceType solidPiece, List<PieceType> blockers)
    {
        Coordinates = coordinates;
        SolidPieceType = solidPiece;
        ForegroundBlockerTypes = blockers;
    }
}

[Serializable]
public abstract class VictoryCondition
{
    public VictoryCondition(int count, List<PieceType> pieceTypes)
    {
        Count = count;
        PieceTypes = pieceTypes;
    }

    public int Count;
    public List<PieceType> PieceTypes;

    public abstract bool CheckState(List<Tile> tiles);
    public abstract void Update(PieceType pieceType);
}

public class CountCondition : VictoryCondition
{
    public CountCondition(int count, List<PieceType> pieceType) : base(count, pieceType) { }

    public override bool CheckState(List<Tile> tiles)
    {
        return Count < 1 ? true : false;
    }

    public override void Update(PieceType pieceType)
    {
        if (PieceTypes.Contains(pieceType))
        {
            Count--;
        }
    }
}

public class InRoomCondition : VictoryCondition
{
    public InRoomCondition(int count, List<PieceType> pieceType) : base(count, pieceType) { }

    public override bool CheckState(List<Tile> tiles)
    {
        Count = 0;

        for (int i = 0; i < tiles.Count; i++)
        {
            Count += tiles[i].AllBlockerPieces.FindAll(p => PieceTypes.Contains(p.PieceType)).Count;

            if (PieceTypes.Contains(tiles[i].SolidPieceType))
                Count++;
        }

        return Count < 1 ? true : false;
    }

    public override void Update(PieceType pieceType) { }
}