﻿using UnityEngine;
using System;

public class CollisionEvent : MonoBehaviour {

    private Action<Collider2D> _listener;

    public void Init(Action<Collider2D> listener)
    {
        _listener = listener;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        _listener?.Invoke(other);
    }
}
