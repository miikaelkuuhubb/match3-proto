﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public TextAsset LevelDataFile;
    public GridData GridData;

    int[] levelData;

    private Vector2[] _directions = { new Vector2(-1, 0), new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, -1) };

    private int _gridWidth;
    private int _gridHeight;

    public void Init()
    {
        GenerateGridOLD();

        /*
        LevelData levelData = DeserializeLevelData(LevelDataFile.text);
        
        GenerateGrid(levelData);
        AssignRooms(levelData.Rooms);
        */
    }

    private LevelData DeserializeLevelData(string jsonData)
    {
        return JsonConvert.DeserializeObject<LevelData>(jsonData);
    }

    private void AssignRooms(List<RoomData> rooms)
    {
        GameManager.instance.rooms = rooms;
        GameManager.instance.currentRoom = rooms[0];
    }

    private void GenerateGrid(LevelData levelData)
    {
        foreach (TileData tile in levelData.Tiles)
        {
            GameObject newTileGameObject = Instantiate(GridData.TilePrefab, transform);

            TileObject newTileObject = newTileGameObject.GetComponent<TileObject>();

            GameObject newPieceGameObject = Instantiate(GridData.PiecePrefab, newTileGameObject.transform);

            SolidPieceObject newPieceObject = newPieceGameObject.GetComponent<SolidPieceObject>();
            newPieceObject.Piece = GridData.GetPieceOfType(tile.SolidPieceType);

            List<BlockerPieceObject> blockers = new List<BlockerPieceObject>();

            Tile newTile = new Tile(tile.Coordinates, newTileObject, GridData, newPieceObject, blockers);

            foreach (PieceType blocker in tile.ForegroundBlockerTypes)
            {
                newTile.CreateBlockerPiece(blocker);
            }

            newTileObject.SetTile(newTile);
            newTileGameObject.transform.localPosition = new Vector2(tile.Coordinates.x * newTileObject.BoardRenderer.bounds.size.x, tile.Coordinates.y * newTileObject.BoardRenderer.bounds.size.x);

            GridData.Tiles.Add(newTile);
            newPieceGameObject.transform.localScale = new Vector3(2, 2, 1);
        }

        AssignNeighbours();
        InitializeTiles();
    }

    private void GenerateGridOLD()
    {
        string[] dataPoints = LevelDataFile.ToString().Split(',');
        levelData = new int[dataPoints.Length];

        for (int i = 0; i < dataPoints.Length; i++)
        {
            levelData[i] = int.Parse(dataPoints[i]);
        }

        _gridWidth = levelData[1];
        _gridHeight = levelData[2];

        GridData.Clear();

        //the first integer in the level file is the index of the start of the grid data
        int index = levelData[0];

        for (int x = 0; x < _gridWidth; x++)
        {
            for (int y = 0; y < _gridHeight; y++)
            {
                if (levelData[index] != -1)
                {
                    GameObject newTileGameObject = Instantiate(GridData.TilePrefab, transform);

                    TileObject newTileObject = newTileGameObject.GetComponent<TileObject>();

                    GameObject newPieceGameObject = Instantiate(GridData.PiecePrefab, newTileGameObject.transform);
                    newPieceGameObject.transform.localPosition = new Vector2(0, 0);

                    SolidPieceObject newPieceObject = newPieceGameObject.GetComponent<SolidPieceObject>();
                    List<BlockerPieceObject> blockers = new List<BlockerPieceObject>();

                    Tile newTile = new Tile(new Vector2(x, y), newTileObject, GridData, newPieceObject, blockers);

                    newTileObject.SetTile(newTile);
                    newTile.SetPiece(PieceType.Empty);
                    newTileGameObject.transform.localPosition = new Vector2(x * newTileObject.BoardRenderer.bounds.size.x, y * newTileObject.BoardRenderer.bounds.size.x);

                    GridData.Tiles.Add(newTile);
                    newPieceGameObject.transform.localScale = new Vector3(2, 2, 1);

                }

                index++;
            }
        }

        AssignNeighbours();
        InitializeTiles();
    }

    public void InitializeTiles()
    {
        for (int i = 0; i < GridData.Tiles.Count; i++)
        {
            GridData.Tiles[i].Create();
        }
    }

    public void AssignNeighbours()
    {
        for (int i = 0; i < GridData.Tiles.Count; i++)
        {
            Tile[] newNeighbours = new Tile[_directions.Length];

            for (int j = 0; j < _directions.Length; j++)
            {
                newNeighbours[j] = GridData.Tiles.Find(t => t.Coordinates == GridData.Tiles[i].Coordinates + _directions[j]);
            }

            GridData.Tiles[i].Neighbours = newNeighbours;
        }
    }

    private Piece GetPieceType(PieceType type)
    {
        return GridData.GetPieceOfType(type);
    }
}