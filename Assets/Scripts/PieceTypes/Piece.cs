﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Piece : ScriptableObject
{
    public int MaxHealth;

    [Header("Piece Variables")]

    /// <summary>
    /// Determines how many times a tile must be hit before it is destroyed.
    /// </summary>
    public PieceType PieceType;

    /// <summary>
    /// Primary sprite of the piece while on board.
    /// </summary>
    public List<Sprite> PieceSprites = new List<Sprite>();

    /// <summary>
    /// Method is triggered when Tile is clicked.
    /// </summary>
    public abstract void Interact(Tile tile);

    /// <summary>
    /// Method is triggered when Tile is next to a Match or Special Piece interaction.
    /// </summary>
    public abstract void AdjacentHit(Tile tile, bool specialHit);

    /// <summary>
    /// Method is triggered when Tile is inside a match or directly triggered by Special Piece.
    /// </summary>
    public abstract void DirectHit(Tile tile, bool specialHit);

    public abstract void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles);

    public abstract void Create(Tile tile);

    public abstract void Destroy(Tile tile);
}