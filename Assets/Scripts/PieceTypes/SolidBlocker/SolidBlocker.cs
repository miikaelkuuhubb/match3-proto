﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SolidBlocker : SolidPiece
{
    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        if (specialHit)
            return;

        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
            tile.Destroy();
        else
            tile.PieceSprite = PieceSprites[MaxHealth - tile.SolidPieceHealth];

    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
            tile.Destroy();
        else
            tile.PieceSprite = PieceSprites[MaxHealth - tile.SolidPieceHealth];
    }
}