﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PopcornSolidBlocker : SolidBlocker
{
    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        if (specialHit)
            return;

        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
        {
            tile.TileObject.StartCoroutine(DestroyAnimation(tile));
        }
        else
        {
            tile.PieceSprite = PieceSprites[MaxHealth - tile.SolidPieceHealth];
            tile.TileObject.StartCoroutine(PopAnimation(tile));
        }
    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        if (!specialHit)
            return;

        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
        {
            tile.TileObject.StartCoroutine(DestroyAnimation(tile));
        }
        else
        {
            tile.PieceSprite = PieceSprites[MaxHealth - tile.SolidPieceHealth];
            tile.TileObject.StartCoroutine(PopAnimation(tile));
        }
    }

    private IEnumerator PopAnimation(Tile tile)
    {
        InputManager.instance.AddInputBlocker();

        yield return new WaitForSeconds(0.5f);

        List<Tile> tilesToPop = tile.GridData.GetTilesWithinRange(tile, MaxHealth - tile.SolidPieceHealth);
        foreach (Tile popTile in tilesToPop)
        {
            popTile.SetPiece(PieceType.Empty);
            popTile.Create();
        }

        InputManager.instance.RemoveInputBlocker();
    }

    private IEnumerator DestroyAnimation(Tile tile)
    {
        InputManager.instance.AddInputBlocker();

        tile.PieceSprite = PieceSprites[MaxHealth - 1];

        yield return new WaitForSeconds(0.5f);

        tile.Destroy();

        InputManager.instance.RemoveInputBlocker();
    }
}