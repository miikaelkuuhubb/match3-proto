using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ColorPiece : SolidPiece
{
    public override void Interact(Tile tile)
    {
        List<Tile> matchedTiles = new List<Tile>();
        List<Tile> adjacentTiles = new List<Tile>();

        tile.Match(tile.SolidPiece, matchedTiles, adjacentTiles);

        if (matchedTiles.Count >= 2)
        {
            if (matchedTiles.Count >= 5)
            {
                AssignSpecialPiece(tile, matchedTiles.Count);
                matchedTiles.Remove(tile);
            }

            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].DirectHit(false);
            }

            for (int i = 0; i < adjacentTiles.Count; i++)
            {
                adjacentTiles[i].AdjacentHit(false);
            }
        }
        else
        {
            Debug.Log("No Match!");
        }
    }

    public override void AdjacentHit(Tile tile, bool specialHit) { }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        for (int i = 0; i < tile.Neighbours.Length; i++)
        {
            if (tile.Neighbours[i] != null)
            {
                if (originPiece == tile.Neighbours[i].SolidPiece && !matchedTiles.Contains(tile.Neighbours[i]))
                {
                    matchedTiles.Add(tile.Neighbours[i]);
                    tile.Neighbours[i].Match(originPiece, matchedTiles, adjacentTiles);
                }
                else if (adjacentTiles != null && !adjacentTiles.Contains(tile.Neighbours[i]))
                {
                    adjacentTiles.Add(tile.Neighbours[i]);
                }
            }
        }
    }

    private void AssignSpecialPiece(Tile tile, int matchCount)
    {
        if (matchCount >= 5 && matchCount <= 6)
        {
            Debug.Log("Rocket!");
            tile.SetPiece(PieceType.RandomRocket);
            tile.Create();
        }
        else if (matchCount >= 7 && matchCount <= 8)
        {
            Debug.Log("Bomb!");
            tile.SetPiece(PieceType.Bomb);
            tile.Create();
        }
        else if (matchCount >= 9)
        {
            Debug.Log("Plasma!");
            PieceType plasmaColor = tile.SolidPieceType;

            switch (plasmaColor)
            {
                case PieceType.Blue:
                    tile.SetPiece(PieceType.PlasmaBlue);
                    break;
                case PieceType.Red:
                    tile.SetPiece(PieceType.PlasmaRed);
                    break;
                case PieceType.Green:
                    tile.SetPiece(PieceType.PlasmaGreen);
                    break;
                case PieceType.Yellow:
                    tile.SetPiece(PieceType.PlasmaYellow);
                    break;
                case PieceType.Orange:
                    tile.SetPiece(PieceType.PlasmaOrange);
                    break;
                case PieceType.LightBlue:
                    tile.SetPiece(PieceType.PlasmaLightBlue);
                    break;
                default:
                    Debug.Log("Wrong PlasmaColorTargetType!");
                    break;
            }

            tile.Create();
        }
    }
}