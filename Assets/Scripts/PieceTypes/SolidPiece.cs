﻿using System.Collections.Generic;
using UnityEngine;

public class SolidPiece : Piece
{
    public override void Interact(Tile tile)
    {
        //TODO: Play basic interact animation here. 
    }

    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
            tile.Destroy();
    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
            tile.Destroy();
    }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles) { }

    public override void Create(Tile tile)
    {
        tile.CreateSolidPiece(PieceType);
        IndicatorManager.instance.CheckAllMatches();
    }

    public override void Destroy(Tile tile)
    {
        //TODO: Call destroy animation and/or particles here.
        tile.SetPiece(PieceType.RandomColor);
        tile.Create();

        //TODO: Adapt system so that Create is called from here and PieceType is set separately
    }
}