﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TrapBlockerPiece : BlockerPiece
{
    public override void Interact(Tile tile)
    {
        if (tile.TopBlockerHealth <= 1)
            return;

        tile.SolidPiece.Interact(tile);
    }

    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        if (specialHit)
            return;

        if (tile.TopBlockerHealth <= 1)
        {
            tile.Destroy();
        }
    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        if (tile.TopBlockerHealth > 1 && !specialHit)
        {
            tile.TopBlockerHealth--;
            tile.TopBlockerSprite = PieceSprites[MaxHealth - tile.TopBlockerHealth];
        }
        else if (tile.TopBlockerHealth <= 1)
        {
            tile.Destroy();
        }
    }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        if (tile.TopBlockerHealth <= 1)
            return;

        if (originPiece == tile.SolidPiece)
        {
            tile.SolidPiece.Match(tile, originPiece, matchedTiles, adjacentTiles);
        }
    }

    public override void Create(Tile tile) { }

    public override void Destroy(Tile tile)
    {
        tile.RemoveBlockerPiece(PieceType);
    }
}