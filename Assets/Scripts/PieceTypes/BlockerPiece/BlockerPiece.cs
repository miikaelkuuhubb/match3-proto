﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BlockerPiece : Piece
{
    public override void Interact(Tile tile)
    {
        //TODO: Play basic interact animation here. 
    }

    public override void AdjacentHit(Tile tile, bool specialHit)
    {

        tile.TopBlockerHealth--;

        if (tile.TopBlockerHealth < 1)
            tile.Destroy();

    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        if (specialHit == false)
        {
            tile.TopBlockerHealth--;

            if (tile.TopBlockerHealth < 1)
                tile.Destroy();
        }
    }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        if (originPiece == tile.SolidPiece && !matchedTiles.Contains(tile))
        {
            tile.SolidPiece.Match(tile, originPiece, matchedTiles, adjacentTiles);
        }
    }

    public override void Create(Tile tile) { }

    public override void Destroy(Tile tile)
    {
        tile.RemoveBlockerPiece(PieceType);
    }
}