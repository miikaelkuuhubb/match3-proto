﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RopeBlockerPiece : BlockerPiece
{
    public override void Interact(Tile tile)
    {
        //TODO: Play basic interact animation here. 
    }

    public override void AdjacentHit(Tile tile, bool specialHit) { }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        if (originPiece == tile.SolidPiece && !matchedTiles.Contains(tile))
        {
            tile.SolidPiece.Match(tile, originPiece, matchedTiles, adjacentTiles);
        }
    }

    public override void Create(Tile tile) { }

    public override void Destroy(Tile tile)
    {
        tile.RemoveBlockerPiece(PieceType);
    }
}