﻿using UnityEngine;
using System.Collections.Generic;
[CreateAssetMenu]
public class RandomPiece : Piece
{
    public List<PieceType> RandomPieces = new List<PieceType>();

    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        throw new System.NotImplementedException();
    }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        throw new System.NotImplementedException();
    }

    public override void Create(Tile tile)
    {
        tile.SetPiece(RandomPieces[Random.Range(0, RandomPieces.Count)]);
        tile.Create();
    }

    public override void Destroy(Tile tile)
    {
        throw new System.NotImplementedException();
    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        throw new System.NotImplementedException();
    }

    public override void Interact(Tile tile)
    {
        throw new System.NotImplementedException();
    }
}