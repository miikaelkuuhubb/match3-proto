﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlasmaBallPiece : SpecialPiece
{
    [Header("Plasma Variables")]
    public PieceType PlasmaTargetPiece;

    public override void ExcecuteSpecial(Tile tile)
    {
        GameObject newSpecialObject = Instantiate(SpecialPrefab, tile.TileObject.transform.position, Quaternion.identity);
        newSpecialObject.GetComponent<PlasmaBallObject>().Activate(tile, PlasmaTargetPiece, PieceSprites?[0], 0); //TODO: the last parameter is special combination, and should present the special combination
    }



}
