﻿using UnityEngine;

[CreateAssetMenu]
public class BombPiece : SpecialPiece
{
    public override void ExcecuteSpecial(Tile tile)
    {
        GameObject newSpecialObject = Instantiate(SpecialPrefab, tile.TileObject.transform.position, Quaternion.identity);
        tile.SetPiece(PieceType.Empty);
        newSpecialObject.GetComponent<BombObject>().Explode();
    }
}
