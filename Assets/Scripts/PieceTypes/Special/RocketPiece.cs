using UnityEngine;

[CreateAssetMenu]
public class RocketPiece : SpecialPiece
{
    public override void ExcecuteSpecial(Tile tile)
    {
        GameObject newSpecialObject = Instantiate(SpecialPrefab, tile.TileObject.transform.position, Quaternion.identity);
        tile.SetPiece(PieceType.Empty);
        newSpecialObject.GetComponent<RocketObject>().Shoot();
    }
}
