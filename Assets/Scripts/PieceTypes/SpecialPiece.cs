using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SpecialPiece : SolidPiece
{
    [Header("Special Variables")]
    public GameObject SpecialPrefab;
    public float CombinationDuration = 1f;
    [SerializeField] private AnimationCurve _combinationCurve;

    private bool _rocketMatched = false;
    private bool _bombMatched = false;
    private bool _plasmaMatched = false;
    private PieceType _savedPlasmaType;

    public override void Interact(Tile tile)
    {
        InputManager.instance.AddInputBlocker();

        List<Tile> matchedTiles = new List<Tile>();

        tile.Match(tile.SolidPiece, matchedTiles);

        if (matchedTiles.Count > 1)
            SpecialMatch(tile, matchedTiles);
        else
            ExcecuteSpecial(tile);
    }

    public override void AdjacentHit(Tile tile, bool specialHit) { }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        tile.SolidPieceHealth--;

        if (tile.SolidPieceHealth < 1)
            ExcecuteSpecial(tile);
    }

    public override void Match(Tile tile, Piece originPiece, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        for (int i = 0; i < tile.Neighbours.Length; i++)
        {
            if (tile.Neighbours[i] != null)
            {
                if (tile.Neighbours[i].SolidPiece.GetType().IsSubclassOf(typeof(SpecialPiece)) && !matchedTiles.Contains(tile.Neighbours[i]))
                {
                    matchedTiles.Add(tile.Neighbours[i]);
                    tile.Neighbours[i].Match(originPiece, matchedTiles, adjacentTiles);
                }
            }
        }
    }

    public virtual void ExcecuteSpecial(Tile tile)
    {
        Debug.Log("Empty Base Method for Exceute Special");

    }

    public void SpecialMatch(Tile tile, List<Tile> matchedTiles)
    {
        tile.TileObject.StartCoroutine(AnimateSpecialMatch(tile, matchedTiles));
    }

    private IEnumerator AnimateSpecialMatch(Tile tile, List<Tile> matchedTiles)
    {
        //TODO: SPECIAL COMBINATION ANIMATION AND EXCECUTION HERE

        foreach (Tile t in matchedTiles)
        {
            if (t != tile)
            {
                if (t.SolidPieceType == PieceType.RocketHorizontal || t.SolidPieceType == PieceType.RocketVertical)
                    _rocketMatched = true;
                else if (t.SolidPieceType == PieceType.Bomb)
                    _bombMatched = true;
                else if (t.SolidPieceType == PieceType.PlasmaBlue || t.SolidPieceType == PieceType.PlasmaRed || t.SolidPieceType == PieceType.PlasmaGreen || t.SolidPieceType == PieceType.PlasmaYellow || t.SolidPieceType == PieceType.PlasmaOrange || t.SolidPieceType == PieceType.PlasmaLightBlue)
                {
                    _plasmaMatched = true;
                    _savedPlasmaType = t.TileObject.GetComponent<PlasmaBallPiece>().PlasmaTargetPiece; //TODO: saved plasmatype should not be the plasmaball itself.
                }
            }
            if (t != tile)
                t.Destroy();
        }

        if (tile.SolidPieceType == PieceType.PlasmaBlue || tile.SolidPieceType == PieceType.PlasmaRed || tile.SolidPieceType == PieceType.PlasmaGreen || tile.SolidPieceType == PieceType.PlasmaYellow || tile.SolidPieceType == PieceType.PlasmaOrange || tile.SolidPieceType == PieceType.PlasmaLightBlue)
        {
            if (_plasmaMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.PlasmaPlasmaObject, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<PlasmaPlasmaObject>().Activate();
            }
            else if (_bombMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.PlasmaPrefab, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<PlasmaBallObject>().Activate(tile, tile.TileObject.GetComponent<PlasmaBallPiece>().PlasmaTargetPiece, PieceSprites?[0], 2);
            } //TODO: the activation should not give solidpiecetype, but pass the current plasmaball's target
            else if (_rocketMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.PlasmaPrefab, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<PlasmaBallObject>().Activate(tile, tile.TileObject.GetComponent<PlasmaBallPiece>().PlasmaTargetPiece, PieceSprites?[0], 1);
            }
        }
        else if (tile.SolidPieceType == PieceType.Bomb)
        {
            if (_plasmaMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.PlasmaPrefab, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<PlasmaBallObject>().Activate(tile, _savedPlasmaType, PieceSprites?[0], 2);
            }
            else if (_bombMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.BombBombObject, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<BombBombObject>().Activate();
            }
            else if (_rocketMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.RocketBombObject, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<RocketBombObject>().Activate();
            }
        }
        else if (tile.SolidPieceType == PieceType.RocketHorizontal || tile.SolidPieceType == PieceType.RocketVertical)
        {
            if (_plasmaMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.PlasmaPrefab, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<PlasmaBallObject>().Activate(tile, _savedPlasmaType, PieceSprites?[0], 1);
            }
            else if (_bombMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.RocketBombObject, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<RocketBombObject>().Activate();
            }
            else if (_rocketMatched)
            {
                GameObject newSpecialObject = Instantiate(tile.GridData.RocketRocketObject, tile.TileObject.transform.position, Quaternion.identity);
                newSpecialObject.GetComponent<RocketRocketObject>().Activate();
            }
        }

        tile.Destroy();
        //ExcecuteSpecial(tile);
        yield return null;
    }
}