﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EmptyPiece : SolidPiece
{
    public override void AdjacentHit(Tile tile, bool specialHit)
    {
        if (!specialHit)
            tile.Destroy();
    }

    public override void DirectHit(Tile tile, bool specialHit)
    {
        tile.Destroy();
    }
}
