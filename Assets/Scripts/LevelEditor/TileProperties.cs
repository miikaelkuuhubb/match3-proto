﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileProperties : MonoBehaviour
{
    public bool Playable;

    public Vector2 Coordinates;

    public bool RoomAssigned;
    public bool GridToolClicked;
    public PieceType PieceType;
    public List<PieceType> BlockerTypes = new List<PieceType>();

    public bool PieceSet;
    public bool RandomColorSet;

    //SpriteRenderer CurrentSprite;

    public SpriteRenderer IndicationSprite;

    public Sprite CurrentColor;

    LevelEditor LE;

    void Start()
    {
        //CurrentSprite = GetComponent<SpriteRenderer>();
        IndicationSprite = transform.GetChild(0).GetComponent<SpriteRenderer>();
        LE = GameObject.Find("_Manager").GetComponent<LevelEditor>();

        SetPiece();

    }

    void Update()
    {
        if (LE.tp != gameObject.GetComponent<TileProperties>() && GridToolClicked)
        {
            GridToolClicked = false;
        }

        if (!Playable)
        {
            IndicationSprite.sprite = LE.IndicationSprites[0];
        }

        if (Playable && !PieceSet)
        {

            ///BASIC PIECES///
            if (PieceType == PieceType.Empty)
            {
                IndicationSprite.sprite = LE.IndicationSprites[1];
            }
            if (PieceType != PieceType.RandomColor) // If a Random Color gets set to something else, set its current color back to NULL
            {
                CurrentColor = null;
            }
            if (PieceType == PieceType.Red)
            {
                IndicationSprite.sprite = LE.ColorSprites[0];
            }
            if (PieceType == PieceType.Blue)
            {
                IndicationSprite.sprite = LE.ColorSprites[1];
            }
            if (PieceType == PieceType.Green)
            {
                IndicationSprite.sprite = LE.ColorSprites[2];
            }
            if (PieceType == PieceType.Yellow)
            {
                IndicationSprite.sprite = LE.ColorSprites[3];
            }
            if (PieceType == PieceType.Orange)
            {
                IndicationSprite.sprite = LE.ColorSprites[4];
            }
            if (PieceType == PieceType.LightBlue)
            {
                IndicationSprite.sprite = LE.ColorSprites[5];
            }
            if (!RandomColorSet && PieceType == PieceType.RandomColor)
            {
                IndicationSprite.sprite = LE.RandomColorSprites[Random.Range(0, 6)];
                CurrentColor = IndicationSprite.sprite;
                RandomColorSet = true;
            }
            //TURN OFF RANDOM COLOR
            if (PieceType != PieceType.RandomColor)
            {
                RandomColorSet = false;
            }

            /// <summary>
            /// SPECIAL PIECES
            /// </summary>
            if (PieceType == PieceType.RocketHorizontal)
            {
                IndicationSprite.sprite = LE.SpecialSprites[0];
            }
            if (PieceType == PieceType.RocketVertical)
            {
                IndicationSprite.sprite = LE.SpecialSprites[1];
            }
            if (PieceType == PieceType.Bomb)
            {
                IndicationSprite.sprite = LE.SpecialSprites[2];
            }
            if (PieceType == PieceType.PlasmaRed)
            {
                IndicationSprite.sprite = LE.SpecialSprites[3];
            }
            if (PieceType == PieceType.PlasmaBlue)
            {
                IndicationSprite.sprite = LE.SpecialSprites[4];
            }
            if (PieceType == PieceType.PlasmaGreen)
            {
                IndicationSprite.sprite = LE.SpecialSprites[5];
            }
            if (PieceType == PieceType.PlasmaYellow)
            {
                IndicationSprite.sprite = LE.SpecialSprites[6];
            }
            if (PieceType == PieceType.PlasmaOrange)
            {
                IndicationSprite.sprite = LE.SpecialSprites[7];
            }
            if (PieceType == PieceType.PlasmaLightBlue)
            {
                IndicationSprite.sprite = LE.SpecialSprites[8];
            }
            if (PieceType == PieceType.RandomRocket)
            {
                IndicationSprite.sprite = LE.SpecialSprites[9];
            }
            if (PieceType == PieceType.RandomPlasma)
            {
                IndicationSprite.sprite = LE.SpecialSprites[10];
            }

            /// <summary>
            /// SOLID BLOCKERS
            /// </summary>
            if (PieceType == PieceType.PaperPile)
            {
                IndicationSprite.sprite = LE.SolidBlockerSprites[0];
            }
            if (PieceType == PieceType.Popcorn)
            {
                IndicationSprite.sprite = LE.SolidBlockerSprites[1];
            }
            if (PieceType == PieceType.LightBulb)
            {
                IndicationSprite.sprite = LE.SolidBlockerSprites[2];
            }

        }
    }

    public void SetPiece()
    {

    }

    public void SetRandomSprite()
    {
        IndicationSprite.sprite = CurrentColor;
    }
}