﻿using System.Collections.Generic;
using UnityEngine;

public class GridSpawnerLE : MonoBehaviour
{
    public StartupLevelEditor LEStart;
    public LevelEditor LE;

    List<Tile> GridData;

    private int _gridWidth = 50;
    private int _gridHeight = 50;

    public GameObject NullTile;

    public void Start()
    {
        BeginGeneration();
    }

    public void BeginGeneration()
    {
        GenerateGrid();
    }

    private void GenerateGrid()
    {
        for (int x = 0; x < _gridWidth; x++)
        {
            for (int y = 0; y < _gridHeight; y++)
            {
                Vector2 spawnPos = transform.position = new Vector2(x, y);

                GameObject tile = Instantiate(NullTile, spawnPos, Quaternion.identity);
                TileProperties tileProperties = tile.GetComponent<TileProperties>();

                LE.tilesSpawned.Add(tileProperties);
                tileProperties.Coordinates = new Vector2(x, y);
            }
        }
    }
}