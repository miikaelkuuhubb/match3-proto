﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class SaveLoad : MonoBehaviour
{

    public string FileName;
    private string filePath;

    public Text LevelNameText;

    public static SaveLoad instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public void LoadLevelData()
    {
        string jsonData = "loll";

        LevelData levelData = JsonConvert.DeserializeObject<LevelData>(jsonData);

    }

    public void SerializeLevelData(List<TileProperties> tileProperties, List<RoomIdentifier> rooms, int moveLimit, int levelNumber, string levelName)
    {
        List<TileData> newTiles = new List<TileData>();

        foreach (TileProperties tile in tileProperties)
        {
            TileData newTileData = new TileData(tile.Coordinates, tile.PieceType, tile.BlockerTypes);
            newTiles.Add(newTileData);
        }

        List<RoomData> newRooms = new List<RoomData>();

        foreach (RoomIdentifier room in rooms)
        {
            List<VictoryCondition> roomConditions = new List<VictoryCondition>();
            VictoryCondition newCondition;

            if (room.VC01C == 0)
            {
                newCondition = new InRoomCondition(room.VC01C, room.VC01);
                roomConditions.Add(newCondition);
            }
            else
            {
                newCondition = new CountCondition(room.VC01C, room.VC01);
                roomConditions.Add(newCondition);
            }

            if (room.VC02C == 0 && !room.VC02.Contains(PieceType.Empty))
            {
                newCondition = new InRoomCondition(room.VC02C, room.VC02);
                roomConditions.Add(newCondition);
            }
            else
            {
                newCondition = new CountCondition(room.VC02C, room.VC02);
                roomConditions.Add(newCondition);
            }

            RoomData newRoom = new RoomData(room.RoomCoordinates, roomConditions);
            newRooms.Add(newRoom);

        }

        LevelData levelData = new LevelData(moveLimit, newRooms, newTiles);

        string jsonString = JsonConvert.SerializeObject(levelData);

        string path = Application.persistentDataPath + "/LevelData/Level" + levelNumber + ".json";
        Debug.Log("AssetPath:" + path);
        File.WriteAllText(path, jsonString);
    }
}