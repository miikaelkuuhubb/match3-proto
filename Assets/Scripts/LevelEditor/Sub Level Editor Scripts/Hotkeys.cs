﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hotkeys : MonoBehaviour
{
    LevelEditor L;

    // Start is called before the first frame update
    void Start()
    {
        L = GetComponent<LevelEditor>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            if (L.ColorStates)
            {
                L.CategoriesSystem = false;
                L.ColorStates = false;
            }
            else
            {
                L.CategoriesSystem = true;
                L.ToolsSystem = false;
                L.ColorStates = true;
                L.SpecialStates = false;
                L.ForegroundBlockerStates = false;
                L.SolidBlockerStates = false;
            }
            L.CheckStates();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            if (L.SpecialStates)
            {
                L.CategoriesSystem = false;
                L.SpecialStates = false;
            }
            else
            {
                L.CategoriesSystem = true;
                L.ToolsSystem = false;
                L.ColorStates = false;
                L.SpecialStates = true;
                L.ForegroundBlockerStates = false;
                L.SolidBlockerStates = false;
            }
            L.CheckStates();
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            if (L.ForegroundBlockerStates)
            {
                L.CategoriesSystem = false;
                L.ForegroundBlockerStates = false;
            }
            else
            {
                L.CategoriesSystem = true;
                L.ToolsSystem = false;
                L.ColorStates = false;
                L.SpecialStates = false;
                L.SolidBlockerStates = false;
                L.ForegroundBlockerStates = true;
            }
            L.CheckStates();
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            if (L.SolidBlockerStates)
            {
                L.CategoriesSystem = false;
                L.SolidBlockerStates = false;
            }
            else
            {
                L.CategoriesSystem = true;
                L.ToolsSystem = false;
                L.ColorStates = false;
                L.SpecialStates = false;
                L.ForegroundBlockerStates = false;
                L.SolidBlockerStates = true;
            }
            L.CheckStates();
        }

        if (L.CategoriesSystem)
        {
            if (L.ColorStates)
            {
                L.SelectedIndicator.sprite = L.ColorSprites[L.ColorID];
                L.SubCategorySelected.transform.position = L.ColorList[L.ColorID].transform.position;
            }
            if (L.SpecialStates)
            {
                L.SelectedIndicator.sprite = L.SpecialSprites[L.SpecialID];
                L.SubCategorySelected.transform.position = L.SpecialList[L.SpecialID].transform.position;
            }
            if (L.SolidBlockerStates)
            {
                L.SelectedIndicator.sprite = L.SolidBlockerSprites[L.SolidBlockerID];
                L.SubCategorySelected.transform.position = L.SolidBlockerList[L.SolidBlockerID].transform.position;
            }

            ///HOTKEYS
            /// 
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 0;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 0;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 0;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 0;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 1;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 1;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 1;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 1;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 2;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 2;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 3;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 3;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 4;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 4;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 5;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 5;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 6;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 6;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 6;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 7;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 6;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 8;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 6;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 9;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Plus))
            {
                if (L.ColorStates)
                {
                    L.ColorID = 6;
                }
                if (L.SpecialStates)
                {
                    L.SpecialID = 10;
                }
                if (L.ForegroundBlockerStates)
                {
                    L.ForegroundBlockerID = 2;
                }
                if (L.SolidBlockerStates)
                {
                    L.SolidBlockerID = 2;
                }
            }
        }
    }
}