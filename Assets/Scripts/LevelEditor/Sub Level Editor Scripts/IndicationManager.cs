﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicationManager : MonoBehaviour
{

    public LevelEditor LE;

    public GameObject MainCatIndicator;
    public GameObject SubCatIndicator;

    // Use this for initialization
    void Start()
    {
        MainCatIndicator = LE.MainCategorySelected.gameObject;
        SubCatIndicator = LE.SubCategorySelected.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        //MainCategorySelected.transform.position = ColorStateObject.transform.position;

        if (LE.CategoriesSystem)
        {
            SubCatIndicator.SetActive(true);

            if (LE.ColorStates)
            {
                MainCatIndicator.transform.position = LE.ColorStateObject.transform.position;
                MainCatIndicator.SetActive(true);
            }
            if (LE.SpecialStates)
            {
                MainCatIndicator.transform.position = LE.SpecialStateObject.transform.position;
                MainCatIndicator.SetActive(true);
            }
            if (LE.ForegroundBlockerStates)
            {
                MainCatIndicator.transform.position = LE.ForegroundBlockerStateObject.transform.position;
                MainCatIndicator.SetActive(true);
            }
            if (LE.SolidBlockerStates)
            {
                MainCatIndicator.transform.position = LE.SolidBlockerStateObject.transform.position;
                MainCatIndicator.SetActive(true);
            }
        }

        if (LE.ToolsSystem)
        {
            SubCatIndicator.SetActive(false);

            if (LE.GridStates)
            {
                MainCatIndicator.transform.position = LE.GridToolObject.transform.position;
            }
            if (LE.EraserStates)
            {
                MainCatIndicator.transform.position = LE.EraserToolObject.transform.position;
            }
            if (LE.DuplicateStates)
            {
                MainCatIndicator.transform.position = LE.DuplicateToolObject.transform.position;
            }
            if (LE.RoomStates)
            {
                MainCatIndicator.transform.position = LE.RoomToolObject.transform.position;
            }
            MainCatIndicator.SetActive(true);
        }

        if (LE.ToolsSystem && !LE.GridStates && !LE.EraserStates & !LE.DuplicateStates && !LE.RoomStates)
        {
            MainCatIndicator.SetActive(false);
            SubCatIndicator.SetActive(false);
        }

        if (!LE.CategoriesSystem && !LE.ToolsSystem)
        {
            MainCatIndicator.SetActive(false);
            SubCatIndicator.SetActive(false);
        }
    }
}