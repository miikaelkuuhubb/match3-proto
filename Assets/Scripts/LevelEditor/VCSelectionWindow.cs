﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VCSelectionWindow : MonoBehaviour
{
    public int RoomID;
    public int VCID;

    public LevelEditor LE;

    public GameObject ClickedObject;
    public string ClickedName;

    // Start is called before the first frame update
    void Start()
    {
        LE = GameObject.Find("_Manager").GetComponent<LevelEditor>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AssignPiece()
    {
        string clickedName = EventSystem.current.currentSelectedGameObject.name;
        ClickedName = clickedName;

        List<PieceType> newPieceTypes = new List<PieceType>();
        newPieceTypes.Add((PieceType)Enum.Parse(typeof(PieceType), clickedName));

        if (clickedName == "RocketHorizontal")
        {
            newPieceTypes.Add(PieceType.RocketVertical);
        }

        if (clickedName == "PlasmaRed")
        {
            newPieceTypes.Add(PieceType.PlasmaBlue);
            newPieceTypes.Add(PieceType.PlasmaGreen);
            newPieceTypes.Add(PieceType.PlasmaYellow);
            newPieceTypes.Add(PieceType.PlasmaOrange);
            newPieceTypes.Add(PieceType.PlasmaLightBlue);
        }

        if (VCID == 0)
        {
            LE.rooms[RoomID - 1].VC01 = newPieceTypes;
        }
        else if (VCID == 1)
        {
            LE.rooms[RoomID - 1].VC02 = newPieceTypes;

        }

        EventSystem.current.currentSelectedGameObject.transform.parent.transform.parent.transform.parent.gameObject.GetComponent<CanvasGroup>().alpha = 0;
        EventSystem.current.currentSelectedGameObject.transform.parent.transform.parent.transform.parent.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;
        AssignSprite();

    }

    public void AssignSprite()
    {
        Image CastedImage = ClickedObject.transform.GetChild(0).GetComponent<Image>();
        Debug.Log("Worked");

        ///
        /// BASIC COLORS
        if (ClickedName == "Red")
        {
            CastedImage.sprite = LE.ColorSprites[0];
        }
        if (ClickedName == "Blue")
        {
            CastedImage.sprite = LE.ColorSprites[1];
        }
        if (ClickedName == "Green")
        {
            CastedImage.sprite = LE.ColorSprites[2];
        }
        if (ClickedName == "Yellow")
        {
            CastedImage.sprite = LE.ColorSprites[3];
        }
        if (ClickedName == "Orange")
        {
            CastedImage.sprite = LE.ColorSprites[4];
        }
        if (ClickedName == "LightBlue")
        {
            CastedImage.sprite = LE.ColorSprites[5];
        }

        ///
        /// SPECIALS

        if (ClickedName == "RocketHorizontal")
        {
            CastedImage.sprite = LE.RandomSpecialSprites[0];
        }
        if (ClickedName == "Bomb")
        {
            CastedImage.sprite = LE.SpecialSprites[2];
        }
        if (ClickedName == "PlasmaRed")
        {
            CastedImage.sprite = LE.RandomSpecialSprites[1];
        }

        ///
        /// SOLID BLOCKERS

        if (ClickedName == "PaperPile")
        {
            CastedImage.sprite = LE.SolidBlockerSprites[0];
        }
        if (ClickedName == "Popcorn")
        {
            CastedImage.sprite = LE.SolidBlockerSprites[1];
        }
        if (ClickedName == "LightBulb")
        {
            CastedImage.sprite = LE.SolidBlockerSprites[2];
        }
    }

}