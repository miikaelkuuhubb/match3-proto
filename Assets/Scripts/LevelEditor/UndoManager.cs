﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UndoManager : MonoBehaviour
{

    public LevelEditor LE;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            Undo.RegisterCompleteObjectUndo(LE, "Tiles");
            Undo.FlushUndoRecordObjects();
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            Undo.PerformRedo();
        }
    }
}