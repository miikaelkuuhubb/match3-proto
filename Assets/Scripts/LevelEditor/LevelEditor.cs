﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelEditor : MonoBehaviour
{
    [Header("Game Settings")]
    public int LevelID;
    public InputField LevelIDSet;

    public string LevelName;
    public InputField LevelNameSet;

    public int MovesLeft;
    public InputField MovesLeftSet;

    [Header("System Booleans")]
    public bool ToolsSystem;
    public bool CategoriesSystem;

    [Header("Tools")]
    public bool GridStates;
    public bool EraserStates;
    public bool DuplicateStates;
    public bool RoomStates;

    [Header("Duplicate System")] // Pen tool clicks on Tile, saves clicked tile properties to properties below
    public PieceType DuplicatePieceType;
    public bool DuplicatePlayable;

    [Header("Room System")]
    public int roomsAmount;
    public List<RoomIdentifier> rooms;
    public List<GameObject> VCPrefabs;
    public GameObject RoomPrefab;
    private Vector3 firstRoom = new Vector3(20.5f, 30.5f, -5);
    public GameObject VCPrefab;
    public RectTransform VCParent;
    public GameObject VictoryConditionSelectionWindow;

    [Header("Categories")]
    public bool ColorStates;
    public bool SpecialStates;
    public bool ForegroundBlockerStates;
    public bool SolidBlockerStates;

    [Header("List of UI Elements")]

    [Header("UI Clickable Buttons")]
    public List<GameObject> ColorList; // UI Clickable Buttons
    public List<GameObject> SpecialList;
    public List<GameObject> ForegroundBlockerList;
    public List<GameObject> SolidBlockerList;

    [Header("Tool UI Buttons")]
    public GameObject GridToolObject;
    public GameObject EraserToolObject;
    public GameObject DuplicateToolObject;
    public GameObject RoomToolObject;

    [Header("Category UI Buttons")]
    public GameObject ColorStateObject;
    public GameObject SpecialStateObject;
    public GameObject ForegroundBlockerStateObject;
    public GameObject SolidBlockerStateObject;

    [Header("UI Sprite List")]
    public Sprite[] TileSprites;
    public Sprite[] ColorSprites;
    public Sprite[] RandomColorSprites;
    public Sprite[] RandomSpecialSprites;
    public Sprite[] IndicationSprites; // BLACK U, GREY EMPTY AND RANDOM COLOR ?
    public Sprite[] SpecialSprites;
    public Sprite[] ForegroundBlockerSprites;
    public Sprite[] SolidBlockerSprites;

    [Header("Indication System")]
    public Image SelectedIndicator;
    public Image MainCategorySelected;
    public Image SubCategorySelected;

    [Header("Hidden Setting Button")]
    public GameObject UpSection;
    public GameObject DownSection;
    public bool IsSectionHidden;
    public Vector2 DownMoveTo = new Vector3(0, 167, 0);
    public Vector2 DownDefPos = new Vector3(0, 0, 0);
    public Image HideSprite;
    public Sprite[] HideSprites;

    public TileProperties tp;
    public GameObject ClickedObject;

    // List for spawned tiles
    [Header("Tiles Spawned for JSON")]
    public List<TileProperties> tilesSpawned;

    /// <summary>
    /// BOOLEAN STATES
    /// </summary>
    public bool UNPLAYABLE;
    public bool EMPTY;
    public bool RANDOM;

    public int ColorID; // ColorID OF ELEMENTS
    public int SpecialID;
    public int ForegroundBlockerID;
    public int SolidBlockerID;

    void Start()
    {
        CheckStates();

        StartCoroutine(CreateFirstRoom());
    }

    public void SaveLevel()
    {

        int moveLimit = int.Parse(MovesLeftSet.text);
        int levelNumber = int.Parse(LevelIDSet.text);
        string levelName = LevelNameSet.text;

        SaveLoad.instance.SerializeLevelData(tilesSpawned, rooms, moveLimit, levelNumber, levelName);
    }

    private IEnumerator CreateFirstRoom()
    {
        yield return new WaitForSeconds(0.5f);
        CreateRoom(tilesSpawned[1080]);
    }

    public void CreateRoom(TileProperties tile)
    {
        Vector3 offset = tile.transform.position;
        offset.x = offset.x - 0.5f;
        offset.y = offset.y + 0.5f;
        offset.z = offset.z - 1;

        GameObject newRoomObj = GameObject.Instantiate(RoomPrefab, offset, Quaternion.identity);
        RoomIdentifier roomID = newRoomObj.GetComponent<RoomIdentifier>();

        newRoomObj.transform.parent = tile.transform;
        tile.RoomAssigned = true;

        roomID.RoomCoordinates = tile.Coordinates;
        roomID.VC01.Add(PieceType.Empty);
        roomID.VC02.Add(PieceType.Empty);
        roomID.VC01C = 0;
        roomID.VC02C = 0;

        rooms.Add(roomID);

        GameObject victoryConditionPrefab = Instantiate(VCPrefab, VCParent);
        victoryConditionPrefab.GetComponent<VCPrefabIdentifier>().RoomNumberID = roomID.roomNumber;
        VCPrefabs.Add(victoryConditionPrefab);

        Text text = victoryConditionPrefab.transform.GetChild(0).GetComponent<Text>();
        text.text = roomID.roomNumber.ToString();

        roomID.VCNumber = victoryConditionPrefab.transform.GetChild(0).GetComponent<Text>();

    }

    public void DeleteRoom()
    {

    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        rooms.RemoveAll(GameObject => GameObject == null);
        VCPrefabs.RemoveAll(GameObject => GameObject == null);

        if (Input.anyKeyDown)
        {
            CheckStates();
        }

        if (!RoomStates)
        {
            VCParent.gameObject.SetActive(false);
            foreach (RoomIdentifier room in rooms)
            {
                room.gameObject.SetActive(false);
            }
        }

        if (RoomStates)
        {
            foreach (RoomIdentifier room in rooms)
            {
                room.gameObject.SetActive(true);
            }
        }

        foreach (RoomIdentifier room in rooms)
        {
            int newRoomNumber = rooms.IndexOf(room) + 1;
            room.GetComponent<RoomIdentifier>().roomNumber = newRoomNumber;
            room.transform.GetChild(0).GetComponent<TextMesh>().text = newRoomNumber.ToString();
        }

        if (CategoriesSystem)
        {
            if (ColorStates)
            {
                SelectedIndicator.sprite = ColorSprites[ColorID];
                SubCategorySelected.transform.position = ColorList[ColorID].transform.position;
            }
            if (SpecialStates)
            {
                SelectedIndicator.sprite = SpecialSprites[SpecialID];
                SubCategorySelected.transform.position = SpecialList[SpecialID].transform.position;
            }
            if (ForegroundBlockerStates)
            {
                SelectedIndicator.sprite = ForegroundBlockerSprites[ForegroundBlockerID];
                SubCategorySelected.transform.position = ForegroundBlockerList[ForegroundBlockerID].transform.position;
            }
            if (SolidBlockerStates)
            {
                SelectedIndicator.sprite = SolidBlockerSprites[SolidBlockerID];
                SubCategorySelected.transform.position = SolidBlockerList[SolidBlockerID].transform.position;
            }

            if (hit.collider != null)
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                ///////// if (hit.tag == Tool) { Tools Boolean Enables and Category Disables } // To be changed when moved Kuuhubb Matching Stories App Repos

                if (Input.GetMouseButton(0))
                {
                    tp = hit.collider.gameObject.GetComponent<TileProperties>();
                    //tp.PieceSet = true;

                    tp.SetPiece();
                }
                if (Input.GetMouseButtonUp(0))
                {
                    tp = null;
                }
            }
        }
        if (ToolsSystem)
        {
            if (Input.GetMouseButton(0))
            {
                if (GridStates)
                {
                    if (!tp.GridToolClicked)
                    {
                        tp.GridToolClicked = true;
                        tp.SetRandomSprite();
                        tp.Playable = !tp.Playable;
                    }
                }
                if (EraserStates)
                {
                    if (tp.Playable)
                    {
                        tp.PieceType = PieceType.Empty;
                    }
                }
                if (DuplicateStates)
                {
                    SelectedIndicator.sprite = tp.IndicationSprite.sprite;
                    DuplicatePieceType = tp.PieceType;
                    DuplicatePlayable = tp.Playable;
                    DuplicateStates = false;
                }
                if (!DuplicateStates && !GridStates && !EraserStates && !RoomStates)
                {
                    if (tp.Playable)
                    {
                        tp.PieceType = DuplicatePieceType;
                        tp.Playable = DuplicatePlayable;
                    }
                }
            }
            if (!hit.collider.GetComponent<TileProperties>().RoomAssigned && Input.GetMouseButtonDown(0))
            {
                if (RoomStates)
                {
                    if (hit.collider.name == "LETile(Clone)")
                    {
                        CreateRoom(hit.collider.GetComponent<TileProperties>());
                    }
                }
            }
            else if (hit.collider.GetComponent<TileProperties>().RoomAssigned && Input.GetMouseButtonDown(1) && rooms.Count > 1)
            {
                if (RoomStates)
                {
                    if (hit.collider.name == "LETile(Clone)")
                    {
                        hit.collider.GetComponent<TileProperties>().RoomAssigned = false;
                        Destroy(VCPrefabs[hit.collider.transform.GetChild(1).GetComponent<RoomIdentifier>().roomNumber - 1]);
                        Destroy(hit.collider.transform.GetChild(1).gameObject);

                    }
                }
            }
        }

        if (CategoriesSystem)
        {
            if (ColorStates)
            {
                if (tp != null)
                {
                    if (tp.Playable)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            if (ColorID == 0)
                            {
                                tp.PieceType = PieceType.Red;
                            }
                            if (ColorID == 1)
                            {
                                tp.PieceType = PieceType.Blue;
                            }
                            if (ColorID == 2)
                            {
                                tp.PieceType = PieceType.Green;
                            }
                            if (ColorID == 3)
                            {
                                tp.PieceType = PieceType.Yellow;
                            }
                            if (ColorID == 4)
                            {
                                tp.PieceType = PieceType.Orange;
                            }
                            if (ColorID == 5)
                            {
                                tp.PieceType = PieceType.LightBlue;
                            }

                            if (ColorID == 6)
                            {
                                tp.PieceType = PieceType.RandomColor;
                            }
                        }
                    }
                }
            }
            if (SpecialStates)
            {
                if (tp != null)
                {
                    if (tp.Playable)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            if (SpecialID == 0)
                            {
                                tp.PieceType = PieceType.RocketHorizontal;
                            }
                            if (SpecialID == 1)
                            {
                                tp.PieceType = PieceType.RocketVertical;
                            }
                            if (SpecialID == 2)
                            {
                                tp.PieceType = PieceType.Bomb;
                            }
                            if (SpecialID == 3)
                            {
                                tp.PieceType = PieceType.PlasmaRed;
                            }
                            if (SpecialID == 4)
                            {
                                tp.PieceType = PieceType.PlasmaBlue;
                            }
                            if (SpecialID == 5)
                            {
                                tp.PieceType = PieceType.PlasmaGreen;
                            }
                            if (SpecialID == 6)
                            {
                                tp.PieceType = PieceType.PlasmaYellow;
                            }
                            if (SpecialID == 7)
                            {
                                tp.PieceType = PieceType.PlasmaOrange;
                            }
                            if (SpecialID == 8)
                            {
                                tp.PieceType = PieceType.PlasmaLightBlue;
                            }
                            if (SpecialID == 9)
                            {
                                tp.PieceType = PieceType.RandomRocket;
                            }
                            if (SpecialID == 10)
                            {
                                tp.PieceType = PieceType.RandomPlasma;
                            }
                        }
                    }
                }
            }
            if (ForegroundBlockerStates)
            {
                if (tp != null)
                {
                    if (tp.Playable)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            if (ForegroundBlockerID == 0)
                            {
                                tp.PieceType = PieceType.Rope;
                            }
                            if (ForegroundBlockerID == 1)
                            {
                                tp.PieceType = PieceType.Trap;
                            }
                            if (ForegroundBlockerID == 2)
                            {
                                tp.PieceType = PieceType.Piece;
                            }
                        }
                    }
                }
            }
            if (SolidBlockerStates)
            {
                if (tp != null)
                {
                    if (tp.Playable)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            if (SolidBlockerID == 0)
                            {
                                tp.PieceType = PieceType.PaperPile;
                            }
                            if (SolidBlockerID == 1)
                            {
                                tp.PieceType = PieceType.Popcorn;
                            }
                            if (SolidBlockerID == 2)
                            {
                                tp.PieceType = PieceType.LightBulb;
                            }
                        }
                    }
                }
            }
        }
    }

    public void Clicked()
    {
        ClickedObject = EventSystem.current.currentSelectedGameObject.gameObject;

        ///// TOOLS /////
        if (ClickedObject.name == "GridBoolean_Tool")
        {
            if (GridStates)
            {
                GridStates = false;
                ToolsSystem = false;
            }
            else
            {
                GridStates = true;
                ToolsSystem = true;
                EraserStates = false;
                DuplicateStates = false;
                CategoriesSystem = false;
                ColorStates = false;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
                RoomStates = false;
            }

            // Remember to change to tags when changing to MASTER project (Refactor Code)
        }
        if (ClickedObject.name == "Eraser_Tool")
        {
            if (EraserStates)
            {
                EraserStates = false;
                ToolsSystem = false;
            }
            else
            {
                EraserStates = true;
                GridStates = false;
                ToolsSystem = true;
                EraserStates = false;
                DuplicateStates = false;
                CategoriesSystem = false;
                ColorStates = false;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
                RoomStates = false;
            }
        }
        if (ClickedObject.name == "Duplicate_Tool")
        {
            if (DuplicateStates)
            {
                DuplicateStates = false;
                ToolsSystem = false;
            }
            else
            {
                DuplicateStates = true;
                ToolsSystem = true;
                EraserStates = false;
                DuplicateStates = false;
                CategoriesSystem = false;
                ColorStates = false;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
                RoomStates = false;
            }
        }
        if (ClickedObject.name == "Room_Tool")
        {
            if (RoomStates)
            {
                VCParent.gameObject.SetActive(false);
                ToolsSystem = false;
                RoomStates = false;
            }
            else
            {
                RoomStates = true;
                ToolsSystem = true;
                EraserStates = false;
                DuplicateStates = false;
                CategoriesSystem = false;
                ColorStates = false;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
                RoomStates = false;
                VCParent.gameObject.SetActive(true);
                HideToTop();
            }
        }
        if (ClickedObject.name != "Room Tool")
        {
            if (!RoomStates)
            {
                VCParent.gameObject.SetActive(false);
                IsSectionHidden = true;
            }
        }

        ///// CATEGORIES /////
        if (ClickedObject.name == "Color_Category")
        {
            if (ColorStates)
            {
                CategoriesSystem = false;
                ColorStates = false;
            }
            else
            {
                CategoriesSystem = true;
                ToolsSystem = false;
                ColorStates = true;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
            }
        }
        if (ClickedObject.name == "Special_Category")
        {
            if (SpecialStates)
            {
                CategoriesSystem = false;
                SpecialStates = false;
            }
            else
            {
                CategoriesSystem = true;
                ToolsSystem = false;
                ColorStates = false;
                SpecialStates = true;
                ForegroundBlockerStates = false;
                SolidBlockerStates = false;
            }
        }
        if (ClickedObject.name == "ForegroundBlocker_Category")
        {
            if (ForegroundBlockerStates)
            {
                CategoriesSystem = false;
                ForegroundBlockerStates = false;
            }
            else
            {
                CategoriesSystem = true;
                ToolsSystem = false;
                ColorStates = false;
                SpecialStates = false;
                SolidBlockerStates = false;
                ForegroundBlockerStates = true;
            }
        }
        if (ClickedObject.name == "SolidBlocker_Category")
        {
            if (SolidBlockerStates)
            {
                CategoriesSystem = false;
                SolidBlockerStates = false;
            }
            else
            {
                CategoriesSystem = true;
                ToolsSystem = false;
                ColorStates = false;
                SpecialStates = false;
                ForegroundBlockerStates = false;
                SolidBlockerStates = true;
            }
        }
        CheckStates();
    }

    public void ObtainColorID()
    {
        CategoriesSystem = true;
        ToolsSystem = false;

        ColorID = int.Parse(ClickedObject.name);
        Debug.Log(ColorID);
    }
    public void ObtainSpecialID()
    {
        CategoriesSystem = true;
        ToolsSystem = false;

        SpecialID = int.Parse(ClickedObject.name);
        Debug.Log(ColorID);
    }
    public void ObtainForegroundBlockerID()
    {
        CategoriesSystem = true;
        ToolsSystem = false;

        ForegroundBlockerID = int.Parse(ClickedObject.name);
        Debug.Log(ColorID);
    }
    public void ObtainSolidBlockerID()
    {
        CategoriesSystem = true;
        ToolsSystem = false;

        SolidBlockerID = int.Parse(ClickedObject.name);
        Debug.Log(ColorID);
    }

    public void CheckStates()
    {
        if (ColorStates)
        {
            foreach (GameObject color in ColorList)
            {
                color.SetActive(true);

                foreach (GameObject special in SpecialList)
                {
                    special.SetActive(false);
                }
                foreach (GameObject foregroundBlocker in ForegroundBlockerList)
                {
                    foregroundBlocker.SetActive(false);
                }
                foreach (GameObject solidBlocker in SolidBlockerList)
                {
                    solidBlocker.SetActive(false);
                }
            }
        }
        if (SpecialStates)
        {
            foreach (GameObject special in SpecialList)
            {
                special.SetActive(true);

                foreach (GameObject color in ColorList)
                {
                    color.SetActive(false);
                }
                foreach (GameObject foregroundBlocker in ForegroundBlockerList)
                {
                    foregroundBlocker.SetActive(false);
                }
                foreach (GameObject solidBlocker in SolidBlockerList)
                {
                    solidBlocker.SetActive(false);
                }
            }
        }
        if (ForegroundBlockerStates)
        {
            foreach (GameObject foregroundBlocker in ForegroundBlockerList)
            {
                foregroundBlocker.SetActive(true);

                foreach (GameObject special in SpecialList)
                {
                    special.SetActive(false);
                }
                foreach (GameObject color in ColorList)
                {
                    color.SetActive(false);
                }
                foreach (GameObject solidBlocker in SolidBlockerList)
                {
                    solidBlocker.SetActive(false);
                }
            }
        }
        if (SolidBlockerStates)
        {
            foreach (GameObject solidBlocker in SolidBlockerList)
            {
                solidBlocker.SetActive(true);

                foreach (GameObject special in SpecialList)
                {
                    special.SetActive(false);
                }
                foreach (GameObject color in ColorList)
                {
                    color.SetActive(false);
                }
                foreach (GameObject foregroundBlocker in ForegroundBlockerList)
                {
                    foregroundBlocker.SetActive(false);
                }
            }
        }

        if (!ToolsSystem)
        {
            GridStates = false;
            EraserStates = false;
            DuplicateStates = false;
            RoomStates = false;
        }

    }

    public void HideSection()
    {
        if (!RoomStates)
        {
            IsSectionHidden = !IsSectionHidden;

            if (IsSectionHidden)
            {
                UpSection.SetActive(false);
                DownSection.transform.localPosition = DownMoveTo;
                HideSprite.sprite = HideSprites[1];
            }
            if (!IsSectionHidden)
            {
                UpSection.SetActive(true);
                DownSection.transform.localPosition = DownDefPos;
                HideSprite.sprite = HideSprites[0];
            }
        }
    }

    public void HideToTop()
    {
        UpSection.SetActive(false);
        DownSection.transform.localPosition = DownMoveTo;
        HideSprite.sprite = HideSprites[1];
    }
    public void ShowToBottom()
    {
        UpSection.SetActive(true);
        DownSection.transform.localPosition = DownDefPos;
        HideSprite.sprite = HideSprites[0];
    }
}