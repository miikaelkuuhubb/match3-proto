﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomIdentifier : MonoBehaviour
{

	public int roomNumber;
	public Vector2 RoomCoordinates;
	public LevelEditor LE;

	public List<PieceType> VC01 = new List<PieceType>();
	public List<PieceType> VC02 = new List<PieceType>();

	public int VC01C;
	public int VC02C;

	public VCPrefabIdentifier VCPrefabIdentity;
	public Text VCNumber;

	// Use this for initialization
	void Start()
	{
		RoomCoordinates = transform.position;

		// Update is called once per frame

	}

	void Update()
	{
		transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = roomNumber.ToString();
		VCNumber.text = roomNumber.ToString();
		VCPrefabIdentity = VCNumber.transform.parent.GetComponent<VCPrefabIdentifier>();
		VCPrefabIdentity.RoomNumberID = roomNumber;
	}

}