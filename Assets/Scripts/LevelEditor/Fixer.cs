﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fixer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(SetOff());		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator SetOff()
    {
        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);

    }
}
