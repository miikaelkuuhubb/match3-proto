﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VCPrefabIdentifier : MonoBehaviour
{
    public int RoomNumberID;
    public int VCID;

    public LevelEditor LE;

    // Start is called before the first frame update
    void Start()
    {
        LE = GameObject.Find("_Manager").GetComponent<LevelEditor>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenVictoryWindow()
    {
        string clickedName = EventSystem.current.currentSelectedGameObject.name;
        VCID = int.Parse(clickedName);
        LE.VictoryConditionSelectionWindow.GetComponent<VCSelectionWindow>().VCID = VCID;
        LE.VictoryConditionSelectionWindow.GetComponent<VCSelectionWindow>().RoomID = RoomNumberID;

        LE.VictoryConditionSelectionWindow.GetComponent<CanvasGroup>().alpha = 1;
        LE.VictoryConditionSelectionWindow.GetComponent<CanvasGroup>().blocksRaycasts = true;
        LE.VictoryConditionSelectionWindow.GetComponent<VCSelectionWindow>().ClickedObject = EventSystem.current.currentSelectedGameObject;

    }

    public void AssignPiece()
    {
        string clickedName = EventSystem.current.currentSelectedGameObject.name;

        LE.rooms[RoomNumberID].VC01.Add((PieceType)Enum.Parse(typeof(PieceType), clickedName));

    }
}