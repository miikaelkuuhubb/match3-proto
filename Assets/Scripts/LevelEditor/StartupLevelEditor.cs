﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartupLevelEditor : MonoBehaviour
{
    public List<TileProperties> tp;

    private void OnTriggerEnter2D(Collider2D other)
    {
        tp.Add(other.GetComponent<TileProperties>());
    }

    private void Update()
    {
        foreach (TileProperties tile in tp)
        {
            tile.PieceType = PieceType.Empty;
            tile.Playable = true;
            tile.SetPiece();
            Destroy(gameObject);
        }
    }

}



