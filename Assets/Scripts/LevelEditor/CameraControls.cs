﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {

    Rigidbody2D rb;
    Camera cam;
    float speed = 150;

    public LevelEditor LE;

	// Use this for initialization
	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
       
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.up * speed);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(-transform.up * speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-transform.right * speed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(transform.right * speed);
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            cam.orthographicSize -= 1;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            cam.orthographicSize += 1;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            cam.orthographicSize = 4.5f;
        }

        if (Input.GetKey(KeyCode.RightCommand) && Input.GetKeyDown(KeyCode.R))
        {
            cam.transform.position = new Vector3(25, 25, -10);
        }
    }
}
