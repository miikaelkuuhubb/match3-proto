﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class VictoryConditions : MonoBehaviour
{
    public GridData GridData;
    List<VictoryCondition> _currentVictoryConditions = new List<VictoryCondition>();

    public static VictoryConditions instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public void Initialize(List<VictoryCondition> victoryConditions)
    {
        _currentVictoryConditions = victoryConditions;
    }

    public void ProgressRoom(List<VictoryCondition> newConditions)
    {
        _currentVictoryConditions = newConditions;
    }

    public void UpdateConditions(PieceType pieceType)
    {
        foreach (VictoryCondition condition in _currentVictoryConditions)
        {
            condition.Update(pieceType);
        }
    }

    public void CheckConditions()
    {
        foreach (VictoryCondition condition in _currentVictoryConditions)
        {
            if (condition.CheckState(GridData.Tiles))
            {
                _currentVictoryConditions.Remove(condition);
            }
        }

        CheckVictory();
    }

    private void CheckVictory()
    {
        if (_currentVictoryConditions.Count < 1)
        {
            GameManager.instance.ProgressRoom();
        }
    }
}