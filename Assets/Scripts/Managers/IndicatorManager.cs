﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class IndicatorManager : MonoBehaviour
{
    [SerializeField] private GridData _gridData;

    public static IndicatorManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public void CheckAllMatches()
    {
        List<Tile> tilesToCheck = _gridData.Tiles.FindAll(t => t.BlockerCount == 0);
        List<Tile> matchedTiles = new List<Tile>();

        foreach (Tile tile in tilesToCheck)
        {
            matchedTiles.Add(tile);
            tile.Match(tile.SolidPiece, matchedTiles);

            if (tile.SolidPiece.GetType() == typeof(ColorPiece))
            {
                AssignColorIndicators(matchedTiles);
            }
            else if (tile.SolidPiece.GetType() == typeof(SpecialPiece))
            {
                AssignSpecialMatchIndicators(matchedTiles);
            }

            tilesToCheck = tilesToCheck.Except(matchedTiles).ToList();
            matchedTiles.Clear();
        }
    }

    private void AssignColorIndicators(List<Tile> matchedTiles)
    {
        if (matchedTiles.Count < 5)
        {
            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].PieceSprite = matchedTiles[i].SolidPiece.PieceSprites?[0];
            }
            return;
        }

        if (matchedTiles.Count >= 9)
        {
            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].PieceSprite = matchedTiles[i].SolidPiece.PieceSprites?[3];
            }
        }
        else if (matchedTiles.Count >= 7)
        {
            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].PieceSprite = matchedTiles[i].SolidPiece.PieceSprites?[2];
            }
        }
        else if (matchedTiles.Count >= 5)
        {
            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].PieceSprite = matchedTiles[i].SolidPiece.PieceSprites?[1];
            }
        }

    }

    private void AssignSpecialMatchIndicators(List<Tile> matchedTiles)
    {

    }
}