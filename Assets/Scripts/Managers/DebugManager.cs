﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager : MonoBehaviour
{

    public GridData GridData;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider == null)
                return;

            if (Input.GetKey(KeyCode.Alpha1))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.RocketHorizontal);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha2))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.RocketVertical);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha3))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.Bomb);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha4))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.RandomPlasma);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha0))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.Blue);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha5))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().CreateBlockerPiece(PieceType.Rope);
            }
            else if (Input.GetKey(KeyCode.Alpha6))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().CreateBlockerPiece(PieceType.Trap);
            }
            else if (Input.GetKey(KeyCode.Alpha7))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.PaperPile);
                tileObject.GetTile().Create();
            }
            else if (Input.GetKey(KeyCode.Alpha8))
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();
                tileObject.GetTile().SetPiece(PieceType.Popcorn);
                tileObject.GetTile().Create();
            }
        }
    }
}