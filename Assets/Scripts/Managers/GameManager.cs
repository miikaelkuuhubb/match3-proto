﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GridGenerator _gridGenerator;
    [SerializeField] private CameraController _cameraController;

    public List<RoomData> rooms = new List<RoomData>();
    public RoomData currentRoom;

    public static GameManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    private void Start()
    {
        _gridGenerator.Init();
    }

    public void ProgressRoom()
    {
        if (rooms.IndexOf(currentRoom) >= rooms.Count - 1)
        {
            End();
            return;
        }

        currentRoom = rooms[rooms.IndexOf(currentRoom) + 1];
        VictoryConditions.instance.ProgressRoom(currentRoom.VictoryConditions);
        _cameraController.LerpCamera(currentRoom.RoomCoordinate);
    }

    public void End()
    {
        Debug.Log("End");
    }
}