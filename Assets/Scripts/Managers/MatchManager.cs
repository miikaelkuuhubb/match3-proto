﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchManager : MonoBehaviour
{
    public static MatchManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public void Match(Tile tile, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        if (tile.SolidPiece.GetType() == typeof(ColorPiece))
        {

        }
        else if (tile.SolidPiece.GetType().IsSubclassOf(typeof(SpecialPiece)))
        {

        }
    }

    private void SpecialMatch()
    {

    }

    public void BasicMatch(Tile tile, List<Tile> matchedTiles, List<Tile> adjacentTiles)
    {
        if (matchedTiles.Count >= 2)
        {
            if (matchedTiles.Count >= 5)
            {
                AssignSpecialPiece(tile, matchedTiles.Count);
                matchedTiles.Remove(tile);
            }

            for (int i = 0; i < matchedTiles.Count; i++)
            {
                matchedTiles[i].DirectHit(false);
            }

            for (int i = 0; i < adjacentTiles.Count; i++)
            {
                adjacentTiles[i].AdjacentHit(false);
            }
        }
        else
        {
            Debug.Log("No Match!");
        }
    }

    public void AssignSpecialPiece(Tile tile, int matchCount)
    {
        if (matchCount >= 5 && matchCount <= 6)
        {
            Debug.Log("Rocket!");
            tile.SetPiece(PieceType.RandomRocket);
        }
        else if (matchCount >= 7 && matchCount <= 8)
        {
            Debug.Log("Bomb!");
            tile.SetPiece(PieceType.Bomb);
        }
        else if (matchCount >= 9)
        {
            Debug.Log("Plasma!");
            PieceType plasmaColor = tile.SolidPieceType;

            switch (plasmaColor)
            {
                case PieceType.Blue:
                    tile.SetPiece(PieceType.PlasmaBlue);
                    break;
                case PieceType.Red:
                    tile.SetPiece(PieceType.PlasmaRed);
                    break;
                case PieceType.Green:
                    tile.SetPiece(PieceType.PlasmaGreen);
                    break;
                case PieceType.Yellow:
                    tile.SetPiece(PieceType.PlasmaYellow);
                    break;
                case PieceType.Orange:
                    tile.SetPiece(PieceType.PlasmaOrange);
                    break;
                case PieceType.LightBlue:
                    tile.SetPiece(PieceType.PlasmaLightBlue);
                    break;
                default:
                    Debug.Log("Wrong PlasmaColorTargetType!");
                    break;
            }
        }
    }
}