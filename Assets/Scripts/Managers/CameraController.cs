﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera mainCamera;
    public AnimationCurve lerpCurve;

    public void Initialize()
    {
        mainCamera = Camera.main;
    }

    public void LerpCamera(Vector3 targetPosition)
    {
        StartCoroutine(LerpCameraRoutine(mainCamera.transform, targetPosition, 1f));
    }

    private IEnumerator LerpCameraRoutine(Transform transformToMove, Vector3 targetPosition, float duration)
    {
        Vector3 startPosition = transformToMove.position;
        float elapsedTime = 0;
        while (elapsedTime <= duration)
        {
            transformToMove.position = Vector3.LerpUnclamped(startPosition, targetPosition, lerpCurve.Evaluate((elapsedTime / duration) * Time.deltaTime));

            yield return null;
        }

        transformToMove.position = targetPosition;
        yield return null;
    }
}