﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private int _inputBlockerCount = 0;
    private bool _inputDisabled = false;
    private Coroutine _inputDisabledRoutine;

    public static InputManager instance = null;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    private void Update()
    {
        if (_inputDisabled)
            return;

        if (Input.touchCount > 0)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);

            if (hit.collider == null)
                return;

            Debug.Log("Touch at " + hit.transform.position);

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                TileObject tileObject = hit.collider.GetComponent<TileObject>();

                if (tileObject == null)
                    return;

                tileObject.Interact();
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider == null)
                return;

            TileObject tileObject = hit.collider.GetComponent<TileObject>();

            if (tileObject == null)
                return;

            tileObject.Interact();
        }
    }

    public void AddInputBlocker()
    {
        _inputBlockerCount++;

        if (_inputDisabledRoutine == null)
        {
            _inputDisabled = true;
            _inputDisabledRoutine = StartCoroutine(InputDisabledRoutine());
        }
    }

    public void RemoveInputBlocker()
    {
        _inputBlockerCount--;

        if (_inputBlockerCount < 0)
        {
            Debug.Log("Input Blockers negative, this shouldn't happen.");
        }
    }

    private IEnumerator InputDisabledRoutine()
    {
        while (_inputBlockerCount > 0)
        {
            yield return null;
        }

        _inputDisabledRoutine = null;
        _inputDisabled = false;
        VictoryConditions.instance.CheckConditions();
    }
}